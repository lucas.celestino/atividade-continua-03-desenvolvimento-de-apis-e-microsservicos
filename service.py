from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50))
    email = db.Column(db.String(50), unique=True)
    
    def __init__(self, name, email):
        self.name = name
        self.email = email

# Criação das tabelas dentro do contexto da aplicação
with app.app_context():
    db.create_all()
    
@app.route('/users', methods=['GET'])
def get_users():
    users = User.query.all()
    output = []
    for user in users:
        user_data = {}
        user_data['id'] = user.id
        user_data['name'] = user.name
        user_data['email'] = user.email
        output.append(user_data)
    return jsonify({'users': output})

@app.route('/users', methods=['POST'])
def add_user():
    name = request.form['name']
    email = request.form['email']
    new_user = User(name, email)
    db.session.add(new_user)
    db.session.commit()
    return jsonify({'Info': 'Usuário Criado com sucesso!'})

@app.route('/users/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):
    user = User.query.get(user_id)
    if not user:
        return jsonify({'Info': 'Usuário não existe!'})
    db.session.delete(user)
    db.session.commit()
    return jsonify({'Info': 'Usuário Deletado com sucesso!'})

if __name__ == '__main__':
    app.run()
